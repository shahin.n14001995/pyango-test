import { rest } from 'msw';

export const handlers = [

  rest.post('/login',(req, res, ctx) => {
    return res(
      sessionStorage.setItem('is-authenticated', 'true'),
      ctx.status(200)
    )
  }),

  rest.get('/user', (req, res, ctx) => {

    const isAuthenticated = sessionStorage.getItem('is-authenticated')
    if (!isAuthenticated) {
      return res(
        ctx.status(403)
        ctx.json({
          errorMessage: 'Not authorized',
        })
      )
    }
    // If authenticated, return a mocked user details
    return res(
      ctx.status(200),
      ctx.json({
        username: 'admin',
      })
    )
  })

]